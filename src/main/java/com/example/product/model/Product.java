package com.example.product.model;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@Entity
public class Product {
    @Id
    @SequenceGenerator(
            name = "product_sequence_generator",
            sequenceName = "product_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "product_sequence_generator"
    )@Column(name = "id", updatable = false)
    Long id;

    @NotBlank( message = "code can not be blank" )
    @Size( message = "code must be exactly 10 characters!", min = 10, max = 10)
    String code;
            @NotBlank(message = "Name can not be blank!")
    String name;
            @Positive(message = "Price must be positive!")
    float priceHrk;
    float priceEur;
    String description;
    boolean isAvailable;
}
