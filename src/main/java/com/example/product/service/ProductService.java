package com.example.product.service;

import com.example.product.model.Product;
import com.example.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final HNBService hnbService;

    @Autowired
    public ProductService(ProductRepository productRepository, HNBService hnbService) {
        this.productRepository = productRepository;
        this.hnbService = hnbService;
    }

    public Optional<Product> findProductById(Long id){
        boolean productExists = productRepository.existsById(id);
        if(!productExists){
           throw new IllegalStateException( "Product with id: " + id + " does not exists!" );
        }
        return productRepository.findById(id);
    }

    public List<Product> finAllProducts(){
        return productRepository.findAll();
    }


    public Product addProduct(Product request){
        boolean codeExists = productRepository.existsByCode(request.getCode());
        if(codeExists){
            throw new IllegalStateException( "Product with code: " + request.getCode() + " exists!" );
        }
        Product product = new Product();
        product.setCode(request.getCode());
        product.setName(request.getName());
        product.setPriceHrk(request.getPriceHrk());
        product.setPriceEur( hnbService.calculator(request.getPriceHrk()));
        product.setDescription(request.getDescription());
        product.setAvailable(request.isAvailable());
        return productRepository.save( product );
    }

    public void deleteProduct(Long id){
        boolean productExists = productRepository.existsById(id);
        if(!productExists){
            throw new IllegalStateException("Product with id: " + id + " does not exists!");
        }
        productRepository.deleteById(id);
    }

    @Transactional
    public Product updateProduct(Long id, Product request) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException("Product with id: " + id + " does not exists!"));
        Product productCode = productRepository.findByCode(request.getCode());

        if(productRepository.existsByCode(request.getCode())){
            if(!Objects.equals(productCode.getId(), product.getId())){
                throw new IllegalStateException("Product with code: " + request.getCode() + " exists!"+
                        " -> id is: " + productCode.getId());
            }
        }
            product.setName(request.getName());
            product.setCode(request.getCode());
            product.setPriceHrk(request.getPriceHrk());
            product.setPriceEur( hnbService.calculator(request.getPriceHrk()));
            product.setDescription(request.getDescription());
            product.setAvailable(request.isAvailable());
        return productRepository.save(product);
    }

}
