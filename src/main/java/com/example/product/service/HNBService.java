package com.example.product.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Service
public class HNBService {

    @Value("${api.hnb.url}")
    String apiUrl;

    public List<Float> getLatestCourse(){
        List<Float> currency = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        String rawJson = restTemplate.getForObject(apiUrl, String.class );
        JSONArray rawArray = new JSONArray( rawJson );

        for(int index=0; index<rawArray.length(); index++){
            JSONObject data = rawArray.getJSONObject(index);
            float euro = replaceComma( data.getString( "Srednji za devize" ) );
            float unit = data.getFloat( "Jedinica" );
            currency.add( euro );
            currency.add( unit );
        }return currency;
    }

    private Float replaceComma(String number){
        return Float.parseFloat(number.replaceAll(",", "."));
    }

    public float calculator(float hrk){
        List<Float> currency = getLatestCourse();
        float eur = currency.get(0);
        float unit = currency.get(1);
        return ( hrk/eur ) * unit;
    }

}
