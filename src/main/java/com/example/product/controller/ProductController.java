package com.example.product.controller;


import com.example.product.model.Product;
import com.example.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/product")
public class ProductController {

    private final ProductService productService;;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> getAllProducts(){
        return productService.finAllProducts();
    }

    @GetMapping(path = "{id}")
    public Optional<Product> getProductById(@PathVariable("id") Long id){
        return productService.findProductById(id);
    }

    @PostMapping
    public Product addProduct( @Valid @RequestBody
                           Product product){
        return productService.addProduct(product);
    }

    @DeleteMapping(path = "{id}")
    public void deleteProduct(@PathVariable("id") Long id){
        productService.deleteProduct(id);
    }

    @PutMapping(path = "{id}")
    public Product updateProduct(@PathVariable("id") Long id,
                               @Valid @RequestBody(required = false) Product product){
        return productService.updateProduct(id, product);
    }

}
