package com.example.product.serviceTest;

import com.example.product.model.Product;
import com.example.product.repository.ProductRepository;
import com.example.product.service.HNBService;
import com.example.product.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @Mock
    private  ProductRepository productRepository;

    @InjectMocks
    private  ProductService productService;

    @Mock
    private HNBService hnbService;

    private Product product;

    @BeforeEach
    void setUp() {
        product = new Product();
        product.setId(1L);
        product.setName("kupus");
        product.setPriceHrk(18);
        product.setCode( "1234567890" );
        product.setAvailable(true);
        product.setDescription("Opis proizvoda je čisto informativne prirode");
    }

    @Test
     void canFindAll(){
             //when
        productService.finAllProducts();

        verify(productRepository).findAll();
    }


     @Test
     void canAddProduct(){
        //given
             //when
         productService.addProduct(product);

             //then
         ArgumentCaptor<Product> productArgumentCaptor =
                 ArgumentCaptor.forClass(Product.class);

         verify(productRepository)
                 .save(productArgumentCaptor.capture());
         Product productCaptured = productArgumentCaptor.getValue();


         assertThat(productCaptured).isEqualTo(product);
    }

    @Test
    void canFindAllProducts(){
        //given

        BDDMockito.given(productRepository.findAll()).willReturn(List.copyOf(Collections.emptyList()));

        //when

        List<Product> listOfProducts = productService.finAllProducts();

        //then

        assertThat(listOfProducts).isEmpty();
        assertThat(listOfProducts.size()).isEqualTo(0);
    }

     @Test
     void canGetProductById(){
        BDDMockito.given(productRepository.save(product)).willReturn(product);

        BDDMockito.given(productRepository.findById(product.getId())).willReturn(Optional.of(product));

         //when

         Product savedProduct = productService.findProductById(product.getId()).get();

         //then

         assertThat(savedProduct).isNotNull();
    }

     @Test
     void canUpdateProduct(){
        //given

         BDDMockito.given(productRepository.save(product)).willReturn(product);

         product.setAvailable(false);
         product.setName("grah");
         product.setPriceEur(10);

         //when

         Product updatedProduct = productService.updateProduct(1L,product);

         //then

         assertThat(updatedProduct.isAvailable()).isEqualTo(false);
         assertThat(updatedProduct.getName()).isEqualTo("grah");
         assertThat(updatedProduct.getPriceHrk()).isEqualTo(10);

    }



}
