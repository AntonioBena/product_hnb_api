package com.example.product.controllerTest;

import com.example.product.controller.ProductController;
import com.example.product.model.Product;
import com.example.product.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@WebMvcTest
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Autowired
    private ObjectMapper objectMapper;

     @Test
     void test_post() throws Exception {
         //given
         Product product = new Product();
         product.setName("luk");
         product.setCode("1234567890");
         product.setPriceHrk(122);
         product.setAvailable(true);
         product.setDescription("nema opisa proizvoda");

         BDDMockito.given(productService.addProduct(any(Product.class)))
                 .willAnswer((invocation)-> invocation.getArgument(0));

          //when
          ResultActions response = mockMvc.perform(MockMvcRequestBuilders.post("/api/product")
                 .contentType(MediaType.APPLICATION_JSON)
                 .content(objectMapper.writeValueAsString(product)));

          //then

         response
                 .andDo(MockMvcResultHandlers.print())
                 .andExpect(MockMvcResultMatchers.status().isOk())
                 .andExpect(MockMvcResultMatchers
                         .jsonPath("$.name", CoreMatchers.is(product.getName())))
                 .andExpect(MockMvcResultMatchers
                         .jsonPath("$.code", CoreMatchers.is(product.getCode())));
     }

      @Test
       void test_get_all() throws Exception {
          //given
          List<Product> listOfProducts = new ArrayList<>();

          for (int index = 0; index <3; index++){
              Product product = new Product();
              product.setId(1L);
              product.setName("luk" + index);
              product.setCode("123456789" +index);
              product.setPriceHrk(122);
              product.setPriceEur(10);
              product.setAvailable(true);
              product.setDescription("nema opisa proizvoda" +index);
              listOfProducts.add(
                      product
              );
          }

          BDDMockito.given(productService.finAllProducts()).willReturn(listOfProducts);

          //when
          ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/product"));

           //then

          response
                  .andDo(MockMvcResultHandlers.print())
                  .andExpect(MockMvcResultMatchers.status().isOk())
                  .andExpect(MockMvcResultMatchers
                          .jsonPath("$.size()", CoreMatchers.is(listOfProducts.size())));
      }

       @Test
        void test_getById() throws Exception {
           //given
           Long id = 1L;
           Product product = new Product();
           product.setId(id);
           product.setName("luk");
           product.setCode("1234567890");
           product.setPriceHrk(122);
           product.setAvailable(true);
           product.setDescription("nema opisa proizvoda");

           BDDMockito.given(productService.findProductById(id)).willReturn(java.util.Optional.of(product));
           //when

           ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/api/product/{id}", id));

            //then
           response
                   .andDo(MockMvcResultHandlers.print())
                   .andExpect(MockMvcResultMatchers.status().isOk());
       }

        @Test
         void test_update() throws Exception {
            //given
            long id = 1L;
            Product savedProduct = new Product();
            savedProduct.setId(id);
            savedProduct.setName("luk");
            savedProduct.setCode("1234567890");
            savedProduct.setPriceHrk(122);
            savedProduct.setAvailable(true);
            savedProduct.setDescription("nema opisa proizvoda");

            Product updatedProduct = new Product();
            updatedProduct.setName("grah");
            updatedProduct.setCode("1234567898");
            updatedProduct.setPriceHrk(12211);
            updatedProduct.setAvailable(true);
            updatedProduct.setDescription("nema opisa proizvoda");

            BDDMockito.given(productService.findProductById(id)).willReturn(java.util.Optional.of(savedProduct));

            BDDMockito.given(productService.updateProduct(id, any(Product.class)))
                    .willAnswer((invocation)-> invocation.getArgument(0));

            //when
            ResultActions response = mockMvc.perform(MockMvcRequestBuilders.put("/api/product/{id}", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(updatedProduct)));

             //then
            response
                    .andDo(MockMvcResultHandlers.print())
                    .andExpect(MockMvcResultMatchers.status().isOk());
        }

         @Test
          void test_delete() throws Exception {
             //given
             long id = 1L;

             BDDMockito.willDoNothing().given(productService).deleteProduct(id);

              //when

             ResultActions response = mockMvc.perform(MockMvcRequestBuilders.delete("/api/product/{id}", id));

              //then
             response
                     .andDo(MockMvcResultHandlers.print())
                     .andExpect(MockMvcResultMatchers.status().isOk());
         }


}
