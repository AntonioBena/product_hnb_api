package com.example.product.repositoryTest;

import com.example.product.model.Product;
import com.example.product.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import java.util.List;
import java.util.Optional;


@DataJpaTest
class RepositoryUnitTest {

    @Autowired
    ProductRepository repository;

    private Product product;

    @BeforeEach
    void setUp() {
        product = new Product();
        product.setName("kupus");
        product.setPriceHrk(18);
        product.setCode( "1234567890" );
        product.setAvailable(true);
        product.setDescription("Opis proizvoda je čisto informativne prirode");
    }

    @Test
    void givenProductObject_when_save_then_returnSaved(){

        //given

        //when
        Product savedProduct = repository.save(product);
        //than

        assertThat(savedProduct).isNotNull();
        assertThat(savedProduct.getId()).isGreaterThan(0);

    }

     @Test
         void givenList_whenFindAll_then_returnSaved(){
             //given

         for( int index = 0; index < 3; index ++){
             Product product = new Product();
             product.setName("product" + index);
             product.setPriceHrk(15 + index);
             product.setCode( "123456789" +index );
             product.setAvailable(true);
             product.setDescription("opis" + index);
             repository.save(product);
         }
             //when

         List<Product> listOfProducts = repository.findAll();

             //then
         assertThat(listOfProducts).isNotNull();
         assertThat(listOfProducts.size()).isEqualTo(3);


         }

         @Test
         void givenProduct_findById(){
             //given

             repository.save(product);

             //when

             Product productById = repository.findById(product.getId()).get();

             //then
             assertThat(productById.getId()).isNotNull();
             assertThat(productById.getId()).isEqualTo(product.getId());

         }

    @Test
    void givenProduct_findByCode(){
        //given

        repository.save(product);

        //when

        String productById = repository.findByCode(product.getCode()).getCode();

        //then
        assertThat(productById).isNotNull();
        assertThat(productById).isEqualTo(product.getCode());

    }

     @Test
     void givenCode_DoesNotExistsByCode(){
        //given

         String code = "1234";

         //when

         boolean exists = repository.existsByCode(code);

         //then
         assertThat(exists).isFalse();
    }

    @Test
    void givenCode_DoesNotExistsById(){
        //given

        long id = 1123;

        //when

        boolean exists = repository.existsById(id);

        //then
        assertThat(exists).isFalse();
    }

    @Test
    void givenProductObject_update_then_returnUpdated(){
        //given

        repository.save(product);
        //when

        Product savedProduct = repository.getById(product.getId());

        savedProduct.setDescription( "grah je poskupio" );
        savedProduct.setPriceHrk(18);
        savedProduct.setAvailable(false);

        //then
        Product updatedProduct = repository.save(savedProduct);

        assertThat(updatedProduct.isAvailable()).isFalse();
        assertThat(updatedProduct.getDescription()).isEqualTo( "grah je poskupio" );
        assertThat(updatedProduct.getPriceHrk()).isEqualTo(18);

    }

     @Test
     void givenProduct_delete(){
         //given

         repository.save(product);
         //when

         repository.delete( product );

         Optional<Product> find = repository.findById(product.getId());

         //then

         assertThat(find).isEmpty();


    }

}
