# Product rest service with hnb api
## Short description
This application is a simple REST service that manages a list of products.
It calculates price from hrk to eur by calling HNB Api

# Setting up database server

## Linux instructions

1. Create the file repository config<br/>
   sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

2. Import repo signing key<br/>
   wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

3. Update packages<br/>
   sudo apt-get update

4. Install latest postgresql (14+)<br/>
   sudo apt-get -y install postgresql

5. Wait for the console to complete the installation, then enter the username and password for the database server

## Microsoft windows instructions

1. Download PostgreSql windows installer<br/>
   https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

2. Install PostgreSQL to machine<br/>
   
3. Wait for the installer to complete the installation, then enter the username and password for the database server

# Setting up database

1. log in to postreSql server<br/>
   sudo -u postgres psql (Linux)<br/>
   open psql shell and follow default questions (database, port, username, password) (Microsoft Windows)

2. create user for product microservice<br/>
   CREATE USER product WITH PASSWORD 'jw8s0F4';

3. Create role<br/>
   CREATE ROLE microservice;

4. Asign role<br/>
   GRANT microservice TO product;

5. create database<br/>
   CREATE DATABASE productdb;

6. grant privileges<br/>
   GRANT ALL PRIVILEGES ON DATABASE productdb to product;
7. added productdb for testing

# Setting up application

## Importing into the Intelij

1.    Open InteliJ
2.    When the welcome screen pops up click open
3.    Search for extracted source of product_hnb_api-main application
4.    Click Ok, click trust project
5.    Wait for InteliJ initialization
6.    On the right side of IInteliJ choose Maven
      then reload all Maven projects in case InteliJ forgets it.
7.    In Maven card choose product -> Lifecycle -> install – this will validate compile and everything else needed for running project
8.    Now in InteliJ toolbar you can click the green button to run the application

## Installing application without importing to the InteliJ

1.    Open Cmd or Linux bash and check for Maven version
      Windows –> mvn –version<br/>
      Linux      ->  sudo mvn –version<br/>
      If you don’t have Maven installed follow Maven install instructions<br/>
2.    Search for extracted source of product_hnb_api-main application and go into the folder
3.    Open cmd or shell inside folder and type mvn install
      Linux with sudo

## Maven install instructions

1.    Go to https://maven.apache.org/download.cgi
2.    Download Maven

Linux<br/>
3.    Unpack Maven<br/>
      Linux with sudo :D -> unzip apache-maven-3.8.4-bin.zip<br/>
      tar xzvf apache-maven-3.8.4-bin.tar.gz
4.    Add the bin directory of the created directory apache-maven-3.8.4 to the PATH environment variable
5.    Confirm with mvn -v in a new shell

Windows

4.    Adding to PATH: Add the unpacked distribution's bin directory to your user PATH environment variable by opening up the system properties (this pc -> properties -> advanced system settings -> environment variables),<br/>
      selecting the Advanced tab, and the Environment Variables button, then adding or selecting the PATH variable in the user variables with the value C:\Program Files\apache-maven-3.8.4\bin.

5.    Confirm with mvn -v in a cmd, you should see Maven home


# Running the application

1. Installation is needed (follow installation instructions w/wo InteliJ)
2. Go to the source folder of product_hnb_api-main
3. Go to the target folder
4. Inside target folder open cmd or shell and type:<br/>

Linux<br/>
sudo java –jar product-0.0.1-SNAPSHOT.jar

Windows<br/>
java –jar product-0.0.1-SNAPSHOT.jar


# REST Endpoints

**{id}** needs to be replaced with the corresponding number of the product id

| Method | URL | DESCRIPTION |
|-----------------|:-------------|:---------------:|
| Get | http://localhost:5050/api/product |Gets all products from database     |
| Get by id | http://localhost:5050/api/product/{id} |Gets product from a database with id                |
| Post    | http://localhost:5050/api/product |Creates product with user request and id               |
| Put     | http://localhost:5050/api/product/{id} |Updates product with user request and id               |
| Delete  | http://localhost:5050/api/product/{id} |Deletes product from a database by id                |

## Example JSON

```json
{
   "code":"1234567810",
   "name":"Kupus",
   "priceHrk": 500,
   "description":"Opis proizvoda je čisto informativne prirode",
   "available": true
}
```




## Authors
Antonio Benković

